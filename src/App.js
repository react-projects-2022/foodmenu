import React, { useState } from "react";
import Menu from "./component/Menu";
import Categories from "./component/Categories";
import items from "./component/data";

const allCategories = ["all", ...new Set(items.map((item) => item.category))];

function App() {
  const [menuIteams, setMenuIteams] = useState(items);
  const [categories, setCategories] = useState(allCategories);

  const filterItems = (category) => {
    if (category === "all") {
      setMenuIteams(items);
      return;
    }
    const newIteam = items.filter((item) => item.category === category);
    setMenuIteams(newIteam);
  };

  return (
    <main>
      <section className="menu section">
        <div className="title">
          <h2>Our Menu</h2>
          <div className="underline"></div>
        </div>
        <Categories categories={categories} filterItems={filterItems} />
        <Menu items={menuIteams} />
      </section>
    </main>
  );
}

export default App;
